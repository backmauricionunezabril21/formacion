package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductoV4Controller {

    @Autowired
    private ProductoRepository repository;

    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }

    /*Get Producto por nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = (List<ProductoMongo>) repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    @GetMapping(value = "/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductosPorPrecio(@PathVariable Double minimo, @PathVariable Double maximo)
    {
        System.out.println(minimo);
        System.out.println(maximo);
        List<ProductoMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    @PostMapping(value="/v4/productos")
    public ResponseEntity<String>addProducto(@RequestBody ProductoMongo productoMongo)
    {
        ProductoMongo resultado = repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @PutMapping(value = "/v4/productos/{id}")
    public ResponseEntity<String> updateProductos(@PathVariable int id, @RequestBody ProductoMongo productoMongo)
    {
       Optional<ProductoMongo> resultado = repository.findById(String.valueOf(id));
       if (resultado.isPresent()) {
           resultado.get().nombre = productoMongo.nombre;
           resultado.get().precio = productoMongo.precio;
           resultado.get().color = productoMongo.color;
           ProductoMongo guardado = repository.save(resultado.get());
           return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
       }
       return new ResponseEntity<String>("Producto no Encontrado", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        /*Posibilidad de buscar el producto, despues eliminarlo si existe*/
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}
