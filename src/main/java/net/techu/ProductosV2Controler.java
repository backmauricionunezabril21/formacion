package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosV2Controler {

    private ArrayList<Producto> listaProductos = null;
    public ProductosV2Controler() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1",27.35));
        listaProductos.add(new Producto(2, "PR2", 18.33));
    }
    /* Get lista de productos */
    @GetMapping(value = "/v2/productos", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }
    @GetMapping("/v2/productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable int id)
    {
        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            //String mensaje = "No se ha encontrado el producto";
            //respuesta = new ResponseEntity(mensaje, HttpStatus.NOT_FOUND);
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }
    /* Add nuevo producto */
    @PostMapping(value = "/v2/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoNuevo) {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(99, nombre, 100.5));
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    /* Add nuevo producto con nombre */
    @PostMapping(value = "/v2/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Producto(99, "NUEVO", 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    @PutMapping("/v2/productos")
    public ResponseEntity<String> updateProducto(@RequestBody Producto cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAModificar = listaProductos.get(cambios.getId());
            System.out.println("Voy a midificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.print("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(cambios.getId(), productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    @DeleteMapping("/v2/productos")
    public ResponseEntity<String> deleteProducto()
    {
        ResponseEntity<String> resultado = null;
        listaProductos.removeAll(listaProductos);
        listaProductos.clear();
        for (Producto p: listaProductos) {
            listaProductos.remove(p);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }
}